Name:           pqiv
Version:        2.13.2
Release:        1
Summary:        A minimalist image viewer inspired by qiv
Group:          Productivity/Graphics/Viewers
License:        GPL v2 or later
Url:            https://github.com/phillipberndt/pqiv
Source0:        https://github.com/phillipberndt/pqiv/archive/%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%if 0%{?suse_version}
BuildRequires:  gtk3-devel poppler-devel libpoppler-glib-devel libspectre-devel ImageMagick-devel libarchive-devel gcc
%endif
%if %{defined fedora}
BuildRequires:  gtk3-devel poppler-devel libspectre-devel ImageMagick-devel libarchive-devel gcc
%endif

%description
Pqiv started as a modern rewrite of Qiv, but evolved into its own image viewer
over time.

Its features include:

 * Command line image viewer
 * Directory traversing to view whole directories
 * Watch files and directories for changes
 * Natural order sorting of the images
 * A status bar showing information on the current image
 * Transparency and animation support
 * Moving, zooming, rotation, flipping
 * Slideshows
 * Highly customizable and scriptable
 * Supports external image filters (e.g. `convert`)
 * Preloads the next image in the background
 * Fade between images
 * Optional PDF/eps/ps support (useful e.g. for scientific plots)
 * Optional video format support (e.g. for webm animations)

%prep
%setup -q -n pqiv-%{version}

%build
./configure --backends-build=shared --libdir=%{_libdir}
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README.markdown
%{_bindir}/pqiv
%{_libdir}/pqiv/*.so
%{_libdir}/pqiv
%{_datadir}/man/man1/pqiv.1*
%{_datadir}/applications/pqiv.desktop

%changelog
* Fri Nov 1 2024 Marco Hartgring <marco.hartgring@gmail.com> - 2.13.2-1
- Updated to 2.13.2, rebuild for Fedora 41

* Mon Aug 26 2024 Marco Hartgring <marco.hartgring@gmail.com> - 2.13.1-2
- Rebuild for Fedora 40

* Tue Mar 19 2024 Marco Hartgring <marco.hartgring@gmail.com> - 2.13.1-1
- Updated to 2.13.1

* Sat Nov 11 2023 Marco Hartgring <marco.hartgring@gmail.com> - 2.12-8
- Rebuild for Fedora 39

* Mon Apr 10 2023 Marco Hartgring <marco.hartgring@gmail.com> - 2.12-7
- Rebuild for Fedora 38

* Sun Aug 22 2021 - Marco Hartgring <marco.hartgring@gmail.com> - 2.12.6
- Updated for SCM builds

* Mon Nov 22 2021 - Marco Hartgring <marco.hartgring@gmail.com> - 2.12.5
- Rebuild for Fedora 35

* Wed Apr 21 2021 - Marco Hartgring <marco.hartgring@gmail.com> - 2.12.4
- spec file cleanup

* Thu Jan 07 2021 - Marco Hartgring <marco.hartgring@gmail.com>
- Updated to v2.12

* Thu Apr 30 2020 - Marco Hartgring <marco.hartgring@gmail.com>
- Rebuild Fedora 32

* Fri Jan 4 2019 - Marco Hartgring <marco.hartgring@gmail.com>
- Updated to v2.11

* Tue Mar 20 2018 - Marco Hartgring <marco.hartgring@gmail.com>
- Updated to v2.10.2

* Tue Mar 28 2017 - Marco Hartgring <marco.hartgring@gmail.com>
- Updated to v2.8.3

* Sun Jul 10 2016 - phillip.berndt@googlemail.com
- initial version
